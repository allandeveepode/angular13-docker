## Imagem Docker do angular13

Partindo do pressuposto que você já tenha o [Docker](https://docs.docker.com/engine/install/ubuntu/) instalado corretamente, para iniciar a instalação das dependências no container use `docker compose -f docker-compose-install.yml up` dentro da pasta "apps", em seguida rode o container da aplicação com `docker compose up -d`.

